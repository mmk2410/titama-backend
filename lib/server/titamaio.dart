library titama.io;

import 'dart:io';
import 'dart:convert';
import 'dart:async';

import '../common/messages.dart';

/**
 * Class for writing and loading the courses as JSON to a file.
 */
class TitamaIo {

  final _filename = "./data.json";

  /**
   * Write courses as JSON in a file.
   * @param List<Courses> _course List of courses to save.
   */
  writeJson(List<Course> _courses) async {
    String _json = JSON.encode(_courses);
    await new File(_filename).writeAsString(_json);
  }

  /**
   * Read a saved json file.
   * @return Future<List<Course>> future with list of courses.
   */
  Future<List<Course>> readJson() async {
    List<Course> courses = new List<Course>();

    File data =  new File(_filename);

    if (await data.exists()) {

      String _content = await data.readAsString();
      Map parsed = JSON.decode(_content);


      for (int i = 0; i < parsed.length; i++) {
        courses.add(new Course.fromJson(parsed[i]));
      }

    }

    return courses;
  }

}
