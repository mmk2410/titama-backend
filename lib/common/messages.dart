library titama.messages;

import 'package:rpc/rpc.dart';

class Course {
  @ApiProperty(required: true)
  String title;

  @ApiProperty(required: true)
  String time;

  @ApiProperty(required: true)
  String day;

  @ApiProperty(defaultValue: "")
  String kind;

  @ApiProperty(defaultValue: "")
  String place;

  @ApiProperty(defaultValue: "")
  String prof;

  @ApiProperty(defaultValue: "")
  String turnin;

  @ApiProperty()
  int id;

  Course();

  String toString() => title.isEmpty ? 'notitle' : title;

  Map toJson() {
    Map map = new Map();
    map["title"] = title;
    map["time"] = time;
    map["day"] = day;
    map["kind"] = kind;
    map["place"] = place;
    map["prof"] = prof;
    map["turnin"] = turnin;
    map["id"] = id;
    return map;
  }

  Course.fromJson(Map courseData) {
    title = courseData["title"];
    time = courseData["time"];
    day = courseData["day"];
    kind = courseData["kind"];
    place = courseData["place"];
    prof = courseData["prof"];
    turnin = courseData["turnin"];
    id = courseData["id"];
  }
}
