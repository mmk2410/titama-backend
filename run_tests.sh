#!/bin/bash

# Tests for the REST Api
# Copright 2016 (c) Marcel Kapfer (mmk2410)
# Licensed under AGPL

TESTS_AMOUNT=0
TESTS_PASSED=0

ERROR_LOG="./error.log"

CONTENT_TYPE="Content-Type: application/json"

BASEURL="http://localhost:8080/titamaApi/v1"

# Delete old log file if it exists
if [ -f $ERROR_LOG ]; then
    rm $ERROR_LOG
fi

# Test for get requests
function test_get {
    if [ -z "$1" ]; then
        echo "[ERROR] No URL give."
        exit 1
    elif [ -z "$2" ]; then
        echo "[ERROR] No expected value given."
        exit 1
    fi

    ((TESTS_AMOUNT++))

    echo "Running test $TESTS_AMOUNT: $1"

    response=$(curl "$1")

    if [ "$2" == "$response" ]; then
        echo "Test $TESTS_AMOUNT passed."
        ((TESTS_PASSED++))
    else
        echo "Test $TESTS_AMOUNT failed."
        echo "Test $TESTS_AMOUNT failed." >> $ERROR_LOG
        echo "$response" >> $ERROR_LOG
    fi
}

# Test for post requests
function test_post {
    if [ -z "$1" ]; then
        echo "[ERROR] No URL give."
        exit 1
    elif [ -z "$2" ]; then
        echo "[ERROR] No post data given."
        exit 1
    elif [ -z "$3" ]; then
        echo "[ERROR] No expected value given."
        exit 1
    fi

    ((TESTS_AMOUNT++))

    echo "Running test $TESTS_AMOUNT: $1"

    response=$(curl -d "$2" -H "$CONTENT_TYPE" "$1")

    if [ "$3" == "$response" ]; then
        echo "Test $TESTS_AMOUNT passed."
        ((TESTS_PASSED++))
    else
        echo "Test $TESTS_AMOUNT failed."
        echo "Test $TESTS_AMOUNT failed." >> $ERROR_LOG
        echo "$response" >> $ERROR_LOG
    fi
}

# Test for delete requests
function test_delete {
    if [ -z "$1" ]; then
        echo "[ERROR] No URL give."
        exit 1
    elif [ -z "$2" ]; then
        echo "[ERROR] No expected value given."
        exit 1
    fi

    ((TESTS_AMOUNT++))

    echo "Running test $TESTS_AMOUNT: $1"

    response=$(curl -X DELETE "$1")

    if [ "$2" == "$response" ]; then
        echo "Test $TESTS_AMOUNT passed."
        ((TESTS_PASSED++))
    else
        echo "Test $TESTS_AMOUNT failed."
        echo "Test $TESTS_AMOUNT failed." >> $ERROR_LOG
        echo "$response" >> $ERROR_LOG
    fi
}

# Test for put requests
function test_put {
    if [ -z "$1" ]; then
        echo "[ERROR] No URL give."
        exit 1
    elif [ -z "$2" ]; then
        echo "[ERROR] No post data given."
        exit 1
    elif [ -z "$3" ]; then
        echo "[ERROR] No expected value given."
        exit 1
    fi

    ((TESTS_AMOUNT++))

    echo "Running test $TESTS_AMOUNT: $1"

    response=$(curl -d "$2" -H "$CONTENT_TYPE" -X PUT "$1")

    if [ "$3" == "$response" ]; then
        echo "Test $TESTS_AMOUNT passed."
        ((TESTS_PASSED++))
    else
        echo "Test $TESTS_AMOUNT failed."
        echo "Test $TESTS_AMOUNT failed." >> $ERROR_LOG
        echo "$response" >> $ERROR_LOG
    fi
}

# Tests
post_course_data='{"title": "FIN Sitzung", "day": "Wed", "time": "18:00"}'
put_course_data='{"title": "FIN Sitzung", "day": "Wed", "time": "18:00", "kind": "meeting"}'

test_get "$BASEURL/courses" "$(cat "./tests/01-get_courses.json")"
test_get "$BASEURL/course/0" "$(cat "./tests/02-get_course-0.json")"
test_get "$BASEURL/course/1" "$(cat "./tests/03-get_course-1.json")"
test_post "$BASEURL/course" "$post_course_data" "$(cat "./tests/04-post_course.json")"
test_get "$BASEURL/courses" "$(cat "./tests/05-get_courses.json")"
test_get "$BASEURL/course/2" "$(cat "./tests/06-get_course-2.json")"
test_delete "$BASEURL/course/1" "$(cat "./tests/07-delete_course-1.json")"
test_get "$BASEURL/courses" "$(cat "./tests/08-get_courses.json")"
test_put "$BASEURL/course/2" "$put_course_data" "$(cat "./tests/09-put_course-2.json")"
test_get "$BASEURL/courses" "$(cat "./tests/10-get_courses.json")"

# Final result
echo "$TESTS_PASSED/$TESTS_AMOUNT tests passed."
