library titama;

import 'dart:io';

import 'package:logging/logging.dart';
import 'package:rpc/rpc.dart';

import '../lib/server/titamaapi.dart';
import '../lib/server/titamaio.dart';
import '../lib/common/messages.dart';

final ApiServer _apiServer = new ApiServer(prettyPrint: true);

main() async {
  Logger.root
    ..level = Level.INFO
    ..onRecord.listen(print);

  // read saved data
  List<Course> courses = await new TitamaIo().readJson();

  _apiServer.addApi(new TitamaApi(courses));

  HttpServer server = await HttpServer.bind(InternetAddress.ANY_IP_V4, 8080);
  server.listen(_apiServer.httpRequestHandler);
  print('Server listening on http://${server.address.host}:${server.port}');
}
