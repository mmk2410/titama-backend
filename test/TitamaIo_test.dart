import 'dart:io';

import 'package:test/test.dart';

import '../lib/common/messages.dart';
import '../lib/server/titamaio.dart';

String jsonContent = '[{"title":"UlmApi","time":"18:00","day":"Monday","kind":"Lab","place":"Weinhof9","prof":"","turnin":"","id":0},{"title":"CCC Ulm","time":"20:00","day":"Monday","kind":"Meeting","place":"Cafe Einstein","prof":"","turnin":"","id":1}]';

void main() {

  group("JSON Serialization", () {

    test("writeJson() writes a course list as a JSON object to a file.", () async {

      List<Course> _courses = new List<Course>();

      Course course = new Course();

      course
        ..title = "UlmApi"
        ..time = "18:00"
        ..day = "Monday"
        ..id = 0
        ..kind = "Lab"
        ..place = "Weinhof9"
        ..prof = ""
        ..turnin = "";

      _courses.add(course);

      course = new Course();
    
      course
        ..title = "CCC Ulm"
        ..time = "20:00"
        ..day = "Monday"
        ..id = 1
        ..kind = "Meeting"
        ..place = "Cafe Einstein"
        ..prof = ""
        ..turnin = "";

      _courses.add(course);

      TitamaIo io = new TitamaIo();

      await io.writeJson(_courses);

      String content = await new File('data.json').readAsString();

      expect(content, equals(jsonContent));

    });

    test("readJson() reads the JSON storage file and parses it", () async {

      final filename = "data.json";

      List<Course> _courses = new List<Course>();

      Course course = new Course();

      course
        ..title = "UlmApi"
        ..time = "18:00"
        ..day = "Monday"
        ..id = 0
        ..kind = "Lab"
        ..place = "Weinhof9"
        ..prof = ""
        ..turnin = "";

      _courses.add(course);

      course = new Course();
    
      course
        ..title = "CCC Ulm"
        ..time = "20:00"
        ..day = "Monday"
        ..id = 1
        ..kind = "Meeting"
        ..place = "Cafe Einstein"
        ..prof = ""
        ..turnin = "";

      _courses.add(course);
      
      await new File(filename).writeAsString(jsonContent);

      TitamaIo io = new TitamaIo();

      List<Course> courses = await io.readJson();

      expect(courses, equals(_courses));
    });
        
  });

}
