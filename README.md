# TiTaMa backend (WIP)

This is the code of the backend of future versions of TiTaMa - a time table manager. It is right now a work in progress project and so not ready for production and not even fully implemented.

It is written in Dart, uses the RPC middleware and SQlite as a database. It is a RESTful API as defined in the definition.raml file.

## License

This software is licensed under [GNU Affero General Public License v3.0](https://www.gnu.org/licenses/agpl-3.0.txt).
